﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusinessModel.Models
{
    public class Calc
    {
        private List<Region> regions;
        private double[] MO;
        private double[] Cov;
        private double[] Var;
        public Calc(List<Region> _regions)
        {
            regions = _regions;
            MO = new double[6];
            Cov = new double[18];
            Var = new double[6];
            Aver();
            Covar();
            Disp();
        }

        private void Aver()
        {
            double[] sum = {0, 0, 0, 0, 0, 0};
            for (int j = 0; j < regions.Count; j++)
            {
                sum[0] += regions[j].PersonCount;
                sum[1] += regions[j].PartOfWorkers;
                sum[2] += regions[j].PartOfInvestment;
                sum[3] += regions[j].SmallInvestment;
                sum[4] += regions[j].Revenue;
                sum[5] += regions[j].MainFunds;
            }

            for (var i = 0; i < 6; i++)
            {
                MO[i] = sum[i] / regions.Count;
            }
        }


        private void Covar()
        {
            double[] sum = new double[15];
            for (int i = 0; i < 15; i++)
            {
                sum[i] = 0;
            }

            for (int i = 0; i < regions.Count; i++)
            {
                sum[0] += (regions[i].PersonCount - MO[0]) * (regions[i].PartOfWorkers - MO[1]);
                sum[1] += (regions[i].PersonCount - MO[0]) * (regions[i].PartOfInvestment - MO[2]);
                sum[2] += (regions[i].PersonCount - MO[0]) * (regions[i].SmallInvestment - MO[3]);
                sum[3] += (regions[i].PersonCount - MO[0]) * (regions[i].Revenue - MO[4]);
                sum[4] += (regions[i].PersonCount - MO[0]) * (regions[i].MainFunds - MO[5]);
                sum[5] += (regions[i].PartOfWorkers - MO[1]) * (regions[i].PartOfInvestment - MO[2]);
                sum[6] += (regions[i].PartOfWorkers - MO[1]) * (regions[i].SmallInvestment - MO[3]);
                sum[7] += (regions[i].PartOfWorkers - MO[1]) * (regions[i].Revenue - MO[4]);
                sum[8] += (regions[i].PartOfWorkers - MO[1]) * (regions[i].MainFunds - MO[5]);
                sum[9] += (regions[i].PartOfInvestment - MO[2]) * (regions[i].SmallInvestment - MO[3]);
                sum[10] += (regions[i].PartOfInvestment - MO[2]) * (regions[i].Revenue - MO[4]);
                sum[11] += (regions[i].PartOfInvestment - MO[2]) * (regions[i].MainFunds - MO[5]);
                sum[12] += (regions[i].SmallInvestment - MO[3]) * (regions[i].Revenue - MO[4]);
                sum[13] += (regions[i].SmallInvestment - MO[3]) * (regions[i].MainFunds - MO[5]);
                sum[14] += (regions[i].Revenue - MO[4]) * (regions[i].MainFunds - MO[5]);
            }

            for (int i = 0; i < 15; i++)
            {
                Cov[i] = sum[i] / regions.Count;
            }
        }

        private void Disp()
        {
            double[] sum = {0, 0, 0, 0, 0, 0};
            for (int i = 0; i < 6; i++)
            {
                sum[0] += (regions[i].PersonCount - MO[0]) * (regions[i].PersonCount - MO[0]);
                sum[1] += (regions[i].PartOfWorkers - MO[1]) * (regions[i].PartOfWorkers - MO[1]);
                sum[2] += (regions[i].PartOfInvestment - MO[2]) * (regions[i].PartOfInvestment - MO[2]);
                sum[3] += (regions[i].SmallInvestment - MO[3]) * (regions[i].SmallInvestment - MO[3]);
                sum[4] += (regions[i].Revenue - MO[4]) * (regions[i].Revenue - MO[4]);
                sum[5] += (regions[i].MainFunds - MO[5]) * (regions[i].MainFunds - MO[5]);
            }

            for (int i = 0; i < 6; i++)
            {
                Var[i] = sum[i] / regions.Count;
            }
        }

        public double[,] CorrMat()
        {
            double[,] lst = new double[6,6];
            for (int i = 0; i < 6; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (i==j)
                    {
                        lst[i, j] = 1;
                    }
                    else
                    {
                        var t = Cov[i + j] / Math.Sqrt(Var[i] * Var[j]);
                        lst[i, j] = t;
                    }
                }
            }

            return lst;
        }

    }
}