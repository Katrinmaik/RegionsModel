﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BusinessModel.Models
{
    public class Region
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PersonCount { get; set; }
        public int PartOfWorkers { get; set; }
        public int PartOfInvestment { get; set; }
        public int SmallInvestment { get; set; }
        public int Revenue { get; set; }
        public int MainFunds { get; set; }
    }
}