﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace BusinessModel.Models
{
    public class RegionContext:DbContext
    {
        public RegionContext()
        { }

        public DbSet<Region> Regions { get; set; }
    }
}