﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BusinessModel.Models;

namespace BusinessModel.Controllers
{
    public class HomeController : Controller
    {
        RegionContext db = new RegionContext();

        public ActionResult Index()
        {

            return View(db.Regions.ToList());
        }
    

    [HttpGet]
        public ActionResult EditRegion(int? id)
        {
            if (id == null)
            {
                return HttpNotFound();
            }

            Region reg = db.Regions.Find(id);
            if (reg != null)
            {
                return View(reg);
            }

            return HttpNotFound();
        }

        [HttpPost]
        public ActionResult EditRegion(Region reg)
        {
            db.Entry(reg).State = EntityState.Modified;
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        [HttpGet]
        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Create(Region reg)
        {
            db.Entry(reg).State = EntityState.Added;
            db.SaveChanges();

            return RedirectToAction("Index");
        }


        [HttpGet]
        public ActionResult Delete(int id)
        {
            Region reg = db.Regions.Find(id);
            if (reg == null)
            {
                return HttpNotFound();
            }

            return View(reg);
        }

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteYes(int id)
        {
            Region reg = db.Regions.Find(id);
            if (reg == null)
            {
                return HttpNotFound();
            }

            db.Regions.Remove(reg);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public ActionResult CorrMatr()
        {
            var lst = db.Regions.ToList();
            Calc calc = new Calc(lst);
            var Corr = calc.CorrMat();
            return View(Corr);
        }

    }
}